// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};


// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        dark_mode: false,
        add_mode: false,
        display: "feed",
        feed: [],
        new_post: "",
        new_reply: "",
        following: false,
        bio: "",
        editing_bio: false,
    };

    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.change_display = function (mode) {
        app.vue.display = mode;
        if (mode == "feed") {
            app.load_feed();
        }
        if(mode == "liked"){
            app.load_liked();
        }
        if(mode == "playlists"){
            app.load_playlist();
        }
    };

    app.load_feed = function () {
        axios.get(get_feed_url, {params: {username: username}}).then(function (response) {
            app.vue.feed = app.enumerate(response.data.feed);
        })
    };

    app.load_liked = function (){
        axios.get(get_liked_url, {params: {username: username}}).then(function (response) {
            app.vue;
        })

    };

     app.load_playlist = function (){
        axios.get(get_p_url, {params: {username: username}}).then(function (response) {
            app.vue;
        })

    };

    app.set_add_status = function (new_status) {
        app.vue.add_mode = new_status;
    };

    app.add_post = function () {
        axios.post(add_post_url,
            {
                content: app.vue.new_post,
            }).then(function (response) {
            app.vue.feed.push({
                id: response.data.id,
                author:response.data.author,
                content: app.vue.new_post,
                num_likes: 0,
                num_reposts: 0,
                num_replies: 0,
                is_author: true,
                name: name,
                username: username,
                liked: false,
                time: response.data.time,
                show_replies: false,
                is_repost: false,
                replies: [],
            });
            app.enumerate(app.vue.feed);
            app.vue.new_post = "";
            app.set_add_status(false);
        });
    };

    app.add_reply = function (post_idx) {
        id = app.vue.feed[post_idx].id
        axios.post(add_post_url,
            {
                content: app.vue.new_reply, quoted_post: id,
            }).then(function (response) {
            app.vue.feed[post_idx].replies.push({
                id: response.data.id,
                author:response.data.author,
                content: app.vue.new_reply,
                num_likes: 0,
                num_reposts: 0,
                num_replies: 0,
                is_author: true,
                name: response.data.name,
                username: response.data.username,
                liked: false,
                reposted: false,
                time: response.data.time,
                show_replies: false,
                replies: [],
            });
            if (username == logged_in_user) {
                app.vue.feed.push({
                    id: response.data.id,
                    author: response.data.author,
                    content: app.vue.new_reply,
                    is_author: true,
                    is_repost: true,
                    liked: false,
                    name: response.data.name,
                    num_likes: 0,
                    num_replies: 0,
                    num_reposts: 0,
                    quoted_name: response.data.quoted_name,
                    quoted_post: id,
                    replies: [],
                    reposted: false,
                    show_replies: false,
                    time: response.data.time,
                    username: response.data.username,
                });
            }
            app.enumerate(app.vue.feed);
            app.enumerate(app.vue.feed[post_idx].replies);
            app.vue.feed[post_idx].num_replies++;
            app.vue.new_reply = "";
        });
    };

    app.delete_post = function (post_idx) {
        let id = app.vue.feed[post_idx].id;
        axios.get(delete_post_url, {params: {id: id}}).then(function (response) {
            for (let i=0; i < app.vue.feed.length; i++) {
                if (app.vue.feed[i].id === id) {
                    app.vue.feed.splice(i, 1);
                    app.enumerate(app.vue.feed);
                    break;
                }
            }
        })
    }

    app.delete_reply = function (post_idx, reply_idx) {
        let id = app.vue.feed[post_idx].replies[reply_idx].id;
        axios.get(delete_post_url, {params: {id: id}}).then(function (response) {
            for (let i=0; i < app.vue.feed.length; i++) {
                if (app.vue.feed[i].id === id) {
                    app.vue.feed.splice(i, 1);
                    app.enumerate(app.vue.feed);
                    break;
                }
            }
            for (let i=0; i < app.vue.feed[post_idx].replies.length; i++) {
                if (app.vue.feed[post_idx].replies[i].id === id) {
                    app.vue.feed[post_idx].replies.splice(i, 1);
                    app.enumerate(app.vue.feed[post_idx].replies);
                    break;
                }
            }
        })
        app.vue.feed[post_idx].num_replies--;
    }

    app.can_post = function () {
        if (username == logged_in_user) {
            return true;
        } else {
            return false;
        }
        return false;
    }

    app.cancel_new_post = function () {
        app.vue.new_post = "";
        app.vue.set_add_status(false);
    }

    app.like_post = function (post_idx) {
        let id = app.vue.feed[post_idx].id;

        if (app.vue.feed[post_idx].liked == false) {
            // updates rating in database
            axios.get(like_post_url, {params: {id: id, liked: 1}})
            // update rating in post_list
            app.vue.feed[post_idx].liked = true;
            app.vue.feed[post_idx].num_likes++;
        } else {
            // updates rating in database
            axios.get(like_post_url, {params: {id: id, liked: 0}})
            // update rating in post_list
            app.vue.feed[post_idx].liked = false;
            app.vue.feed[post_idx].num_likes--;
        }

    }

    app.replies = function (post_idx) {
        let id = app.vue.feed[post_idx].id;
        if (app.vue.feed[post_idx].show_replies == true) {
            app.vue.feed[post_idx].show_replies = false;
        } else {
            for (i=0; i<app.vue.feed.length; i++) {
                app.vue.feed[i].show_replies = false;
            }
            app.vue.feed[post_idx].show_replies = true;
            app.vue.new_reply = "";
            axios.get(get_replies_url, {params: {id: id}}).then( function (response) {
                app.vue.feed[post_idx].replies = app.enumerate(response.data.replies)
            })
        }

    }

    app.repost = function (post_idx) {
        id = app.vue.feed[post_idx].id;
        axios.post(add_post_url, {quoted_post: id}).then( function (response) {
            app.vue.feed[post_idx].reposted = true;
            app.vue.feed[post_idx].num_reposts++;
            if (username == logged_in_user) {
                app.vue.feed.push({
                    id: id,
                    author: app.vue.feed[post_idx].author,
                    content: app.vue.feed[post_idx].content,
                    is_author: app.vue.feed[post_idx].is_author,
                    is_repost: true,
                    liked: app.vue.feed[post_idx].liked,
                    name: app.vue.feed[post_idx].name,
                    num_likes: app.vue.feed[post_idx].num_likes,
                    num_replies: app.vue.feed[post_idx].num_replies,
                    num_reposts: app.vue.feed[post_idx].num_reposts,
                    quoted_name: app.vue.feed[post_idx].quoted_name,
                    quoted_post: app.vue.feed[post_idx].quoted_post,
                    replies: app.vue.feed[post_idx].replies,
                    reposted: app.vue.feed[post_idx].reposted,
                    show_replies: false,
                    time: app.vue.feed[post_idx].time,
                    username: app.vue.feed[post_idx].username,
                });
            }
            app.enumerate(app.vue.feed);
        })
    }

    app.delete_repost = function(post_idx) {
        id = app.vue.feed[post_idx].id
        axios.get(delete_repost_url, {params: {id: id}}).then( function (response) {

            for (let i=0; i < app.vue.feed.length; i++) {
                if (app.vue.feed[i].id === id) {
                    app.vue.feed[i].reposted = false;
                    app.vue.feed[i].num_reposts--;
                    if (app.vue.feed[i].is_repost) {
                        app.vue.feed.splice(i, 1);
                        app.enumerate(app.vue.feed);
                    }
                }
            }
            app.enumerate(app.vue.feed);
        })
    }

    app.follow = function() {
        axios.post(follow_url, {username: username}).then(function (response) {
            app.vue.following = true;
        });
    }

    app.unfollow = function() {
        axios.post(unfollow_url, {username: username}).then(function (response) {
            app.vue.following = false;
        });
    }

    app.edit_bio = function () {
        if(app.vue.editing_bio === false) {
            app.vue.editing_bio = true;
        } else {
            axios.post(update_bio_url, {new_bio: app.vue.bio}).then( function (response) {
                app.vue.editing_bio = false;
            })
        }
    }

     app.upload_file = function (event,) {
        let input = event.target;
        let file = input.files[0];
        if (file) {
            let reader = new FileReader();
            reader.addEventListener("load", function () {
                // Sends the image to the server.
                axios.post(upload_profile_pic_url,
                    {

                        profile_pic: reader.result,
                    })
                    .then(function () {
                        // Sets the local preview.
                       // row.profile_pic = reader.result;

                    });
            });
            reader.readAsDataURL(file);
        }
    };

    // This contains all the methods.
    app.methods = {
        change_display: app.change_display,
        load_feed: app.load_feed,
        set_add_status: app.set_add_status,
        add_post: app.add_post,
        delete_post: app.delete_post,
        can_post: app.can_post,
        cancel_new_post: app.cancel_new_post,
        like_post: app.like_post,
        replies: app.replies,
        add_reply: app.add_reply,
        delete_reply: app.delete_reply,
        repost: app.repost,
        delete_repost: app.delete_repost,
        load_liked: app.load_liked,
        follow: app.follow,
        unfollow: app.unfollow,
        edit_bio: app.edit_bio,
        upload_file : app.upload_file,
        load_playlist:app.load_playlist,
        // Complete as you see fit.

    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        // Typically this is a server GET call to load the data.
        app.load_feed()
        app.vue.following = (following_string === 'true');
        app.vue.bio = bio;
        app.load_playlist()
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code i
init(app);

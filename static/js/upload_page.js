// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};


// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        // Complete as you see fit.
        song_name: "",
        artist: "",
        album: "",
        song_list: [],
    };

    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.reset_form = function(){
        app.vue.song_name = "";
        app.vue.artist = "";
        app.vue.album = "";
    }

    app.add_song = function(){
        axios.post(add_song_url,
            {
                song_name: app.vue.song_name,
                artist: app.vue.artist,
                album: app.vue.album,
            }).then(function (response) {
                app.vue.song_list.push({
                    id: response.data.id,
                });
                app.reset_form();
            });
    };


    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        add_song: app.add_song,

    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        // Typically this is a server GET call to load the data.
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code i
init(app);

// js page for playlist page
// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};


// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        // Complete as you see fit.
        new_playlist: false,
        playlist_name: "",
        bio: "",
        playlist_list: [],
        new_song: false,
        song_list:[],
        current_p_idx: -1,
    };

    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++; e.songs = [];});
        return a;
    };

    app.reset_form = function(){
        app.vue.new_playlist = false;
        app.vue.playlist_name = "";
        app.vue.bio = "";
    }

    app.set_add_status = function(new_status){
        app.vue.new_playlist = new_status;
    };

    app.add_playlist = function(){
        console.log("debug 1");
        axios.post(add_playlist_url,
            {
                playlist_name: app.vue.playlist_name,
                bio: app.vue.bio,
            }).then(function (response) {
                app.vue.playlist_list.push({
                    id: response.data.id,
                    playlist_name: app.vue.playlist_name,
                    bio: app.vue.bio,
                });
                app.reset_form();
                app.set_add_status(false);
            });
            
    };

    app.set_add_newsong_status = function(new_status, p_idx){
        console.log("in new song status")
        console.log(new_status)
        if(new_status == true){
            app.vue.new_song = true;
            app.vue.current_p_idx = p_idx;
        }
        if(new_status == false){
            app.vue.current_p_idx = -1;
        }
    };

    app.select_song = function(song_idx, p_idx,song_name){
        let song = app.vue.song_list[song_idx];
        let playlist = app.vue.playlist_list[p_idx];
        app.vue.current_p_idx = p_idx;
        axios.post(add_songinplaylist_url, {song_id: song.id, playlist: playlist.id});
        app.vue.new_song = false;
        app.update_songinplaylist();
    };

    app.update_songinplaylist = function(){
        //gets songs for every playlist
        x = 0;
        for(i in app.vue.playlist_list){
            console.log(i); 
            let p = app.vue.playlist_list[i];
            console.log(p.id)
            axios.post(get_songsinplay_url, {playlist_id: p.id}).
                then(function (response){
                    console.log(response.data.songs);
                    console.log(x)
                    app.vue.playlist_list[x++].songs = response.data.songs;
                });
        }
    };

    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        set_add_status: app.set_add_status,
        add_playlist: app.add_playlist,
        set_add_newsong_status: app.set_add_newsong_status,
        select_song: app.select_song,
        update_songinplaylist: app.update_songinplaylist,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        // Typically this is a server GET call to load the data.
        axios.get(get_playlists_url).
            then(function (response) {
                app.vue.playlist_list = app.enumerate(response.data.playlists);
                app.update_songinplaylist();
            });
        axios.get(get_all_songs_url).
            then(function(response){
                app.vue.song_list = app.enumerate(response.data.songs);
            })
        

    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code i
init(app);

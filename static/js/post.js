// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};


// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        dark_mode: false,
        post: {},
        new_reply: "",
    };

    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.add_reply = function () {
        id = app.vue.post.id
        axios.post(add_post_url,
            {
                content: app.vue.new_reply, quoted_post: postid,
            }).then(function (response) {
            app.vue.post.replies.push({
                id: response.data.id,
                author:response.data.author,
                content: app.vue.new_reply,
                num_likes: 0,
                num_reposts: 0,
                num_replies: 0,
                is_author: true,
                name: response.data.name,
                username: response.data.username,
                liked: false,
                time: response.data.time,
                show_replies: false,
                replies: [],
            });
            app.enumerate(app.vue.post.replies);
            app.vue.post.num_replies++;
            app.vue.new_reply = "";
        });
    };

    app.delete_post = function (post_idx) {
        let id = app.vue.post.id;
        axios.get(delete_post_url, {params: {id: id}}).then(function (response) {
            for (let i=0; i < app.vue.feed.length; i++) {
                if (app.vue.feed[i].id === id) {
                    app.vue.feed.splice(i, 1);
                    app.enumerate(app.vue.feed);
                    break;
                }
            }
        })
    }

    app.delete_reply = function (reply_idx) {
        let id = app.vue.post.replies[reply_idx].id;
        axios.get(delete_post_url, {params: {id: id}}).then(function (response) {
            for (let i=0; i < app.vue.post.replies.length; i++) {
                if (app.vue.post.replies[i].id === id) {
                    app.vue.post.replies.splice(i, 1);
                    app.enumerate(app.vue.post.replies);
                    break;
                }
            }
        })
        app.vue.post.num_replies--;
    }

    app.like_post = function () {
        if (app.vue.post.liked == false) {
            // updates rating in database
            axios.get(like_post_url, {params: {id: postid, liked: 1}})
            // update rating in post_list
            app.vue.post.liked = true;
            app.vue.post.num_likes++;
        } else {
            // updates rating in database
            axios.get(like_post_url, {params: {id: postid, liked: 0}})
            // update rating in post_list
            app.vue.post.liked = false;
            app.vue.post.num_likes--;
        }

    }

    app.load_post = function () {
        axios.get(get_post_url, {params: {postid: postid}}).then( function (response) {
            app.vue.post = response.data.post;
        });
        axios.get(get_replies_url, {params: {id: postid}}).then( function (response) {
                app.vue.post.replies = app.enumerate(response.data.replies)
        });
    }

    // This contains all the methods.
    app.methods = {
        delete_post: app.delete_post,
        like_post: app.like_post,
        add_reply: app.add_reply,
        delete_reply: app.delete_reply,
        load_post: app.load_post,
        // Complete as you see fit.

    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        // Typically this is a server GET call to load the data.
        app.load_post()
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code i
init(app);

// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};

// //

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.


// NOTE: All of this code is from profile.js since we need to be able to
// make a post from the index page as  well.

let init = (app) => {

    // This is the Vue data.
    app.data = {
        add_mode: false,
        add_new_post: "",
        feed:[],
        display:"feed",
        add_new_reply:"",
    };

    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.change_display = function (mode) {
        app.vue.display = mode;
        if (mode == "feed") {
            app.load_feed();
            //app.follower_feed();
        }
        if(mode == "liked"){
            app.load_liked();
        }
    };

    app.load_feed = function () {
        // axios.get(get_feed_url, {params: {username: username}}).then(function (response) {
        //     app.vue.feed = app.enumerate(response.data.feed);
        // })
        axios.get(follower_feed_url, {params: {username: username}}).then(function (response) {
            app.vue.feed = app.enumerate(response.data.all_posts_sorted);
        })
    };

    app.set_add_post = function(new_status){
        app.vue.add_mode = new_status;
    };

    app.add_post = function () {
        axios.post(add_post_url,
            {
                content: app.vue.add_new_post,
            }).then(function (response) {
            app.vue.feed.push({
                id: response.data.id,
                author:response.data.author,
                content: app.vue.add_new_post,
                num_likes: 0,
                num_reposts: 0,
                num_replies: 0,
                is_author: true,
                name: name,
                username: username,
                liked: false,
                time: response.data.time,
                show_replies: false,
                replies: [],
            });
            app.enumerate(app.vue.feed);
            app.vue.add_new_post = "";
            app.set_add_post(false);
        });
    };

    app.reset_form = function () {
        app.vue.add_new_post = "";
        app.vue.set_add_post(false);
    };

    app.can_post = function () {
        if (username == logged_in_user) {
            return true;
        } else {
            return false;
        }
        return false;
    };
    
    app.like_post = function (post_idx) {
        let id = app.vue.feed[post_idx].id;

        if (app.vue.feed[post_idx].liked == false) {
            // updates rating in database
            axios.get(like_post_url, {params: {id: id, liked: 1}})
            // update rating in post_list
            app.vue.feed[post_idx].liked = true;
            app.vue.feed[post_idx].num_likes++;
        } else {
            // updates rating in database
            axios.get(like_post_url, {params: {id: id, liked: 0}})
            // update rating in post_list
            app.vue.feed[post_idx].liked = false;
            app.vue.feed[post_idx].num_likes--;
        }

    };

    app.delete_post = function (post_idx) {
        let id = app.vue.feed[post_idx].id;
        axios.get(delete_post_url, {params: {id: id}}).then(function (response) {
            for (let i=0; i < app.vue.feed.length; i++) {
                if (app.vue.feed[i].id === id) {
                    app.vue.feed.splice(i, 1);
                    app.enumerate(app.vue.feed);
                    break;
                }
            }
        })
    };

    app.add_reply = function (post_idx) {
        id = app.vue.feed[post_idx].id
        axios.post(add_post_url,
            {
                content: app.vue.add_new_reply, quoted_post: id,
            }).then(function (response) {
            app.vue.feed[post_idx].replies.push({
                id: response.data.id,
                author:response.data.author,
                content: app.vue.add_new_reply,
                num_likes: 0,
                num_reposts: 0,
                num_replies: 0,
                is_author: true,
                name: response.data.name,
                username: response.data.username,
                liked: false,
                time: response.data.time,
                show_replies: false,
                replies: [],
            });
            app.enumerate(app.vue.feed[post_idx].replies);
            app.vue.feed[post_idx].num_replies++;
            app.vue.add_new_reply = "";
        });
    };

    app.delete_reply = function (post_idx, reply_idx) {
        let id = app.vue.feed[post_idx].replies[reply_idx].id;
        axios.get(delete_post_url, {params: {id: id}}).then(function (response) {
            for (let i=0; i < app.vue.feed.length; i++) {
                if (app.vue.feed[i].id === id) {
                    app.vue.feed.splice(i, 1);
                    app.enumerate(app.vue.feed);
                    break;
                }
            }
            for (let i=0; i < app.vue.feed[post_idx].replies.length; i++) {
                if (app.vue.feed[post_idx].replies[i].id === id) {
                    app.vue.feed[post_idx].replies.splice(i, 1);
                    app.enumerate(app.vue.feed[post_idx].replies);
                    break;
                }
            }
        })
        app.vue.feed[post_idx].num_replies--;
    };

    app.replies = function (post_idx) {
        let id = app.vue.feed[post_idx].id;
        if (app.vue.feed[post_idx].show_replies == true) {
            app.vue.feed[post_idx].show_replies = false;
        } else {
            for (i=0; i<app.vue.feed.length; i++) {
                app.vue.feed[i].show_replies = false;
            }
            app.vue.feed[post_idx].show_replies = true;
            app.vue.new_reply = "";
            axios.get(get_replies_url, {params: {id: id}}).then( function (response) {
                app.vue.feed[post_idx].replies = app.enumerate(response.data.replies)
            })
        }

    };

    app.repost = function (post_idx) {
        id = app.vue.feed[post_idx].id;
        axios.post(add_post_url, {quoted_post: id}).then( function (response) {
            app.vue.feed[post_idx].reposted = true;
            app.vue.feed[post_idx].num_reposts++;
            if (username == logged_in_user) {
                app.vue.feed.push({
                    id: id,
                    author: app.vue.feed[post_idx].author,
                    content: app.vue.feed[post_idx].content,
                    is_author: app.vue.feed[post_idx].is_author,
                    is_repost: true,
                    liked: app.vue.feed[post_idx].liked,
                    name: app.vue.feed[post_idx].name,
                    num_likes: app.vue.feed[post_idx].num_likes,
                    num_replies: app.vue.feed[post_idx].num_replies,
                    num_reposts: app.vue.feed[post_idx].num_reposts,
                    quoted_name: app.vue.feed[post_idx].quoted_name,
                    quoted_post: app.vue.feed[post_idx].quoted_post,
                    replies: app.vue.feed[post_idx].replies,
                    reposted: app.vue.feed[post_idx].reposted,
                    show_replies: false,
                    time: app.vue.feed[post_idx].time,
                    username: app.vue.feed[post_idx].username,
                });
            }
            app.enumerate(app.vue.feed);
        })
    }

    app.delete_repost = function(post_idx) {
        id = app.vue.feed[post_idx].id
        axios.get(delete_repost_url, {params: {id: id}}).then( function (response) {

            for (let i=0; i < app.vue.feed.length; i++) {
                if (app.vue.feed[i].id === id) {
                    app.vue.feed[i].reposted = false;
                    app.vue.feed[i].num_reposts--;
                    if (app.vue.feed[i].is_repost) {
                        app.vue.feed.splice(i, 1);
                        app.enumerate(app.vue.feed);
                    }
                }
            }
            app.enumerate(app.vue.feed);
        })
    };
    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        set_add_post:app.set_add_post,
        add_post:app.add_post,
        reset_form:app.reset_form,
        can_post:app.can_post,
        like_post:app.like_post,
        delete_post: app.delete_post,
        add_reply: app.add_reply,
        delete_reply: app.delete_reply,
        replies: app.replies,
        repost: app.repost,
        delete_repost: app.delete_repost,
        change_display: app.change_display,
        load_feed: app.load_feed,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        // Typically this is a server GET call to load the data.
        app.load_feed();
        app.vue.following=(following_string == 'true');
        //app.follower_feed();
        app.vue.following=(following_string == 'true');
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code i
init(app);
